package tech.mastertech.webspringeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer

public class WebSpringEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSpringEurekaApplication.class, args);
	}

}
