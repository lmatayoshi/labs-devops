package tech.mastertech.webspringeurekaclient;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusApplication {

  @RequestMapping("/")
  public String home() {
    return "Client API";
  }

  @RequestMapping("/available")
  public String available() {
    return "Teste - AVAILABLE 01";
  }
  @RequestMapping("/version")
  public String checkedOut() {
    return "Teste 18/3 - Version 2.1";
  }

}
