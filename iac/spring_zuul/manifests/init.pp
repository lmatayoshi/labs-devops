# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include spring_zuul
class spring_zuul {

file { '/opt/apps/zuul':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/zuul.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/spring_zuul/zuul.service',
     }
file { "/opt/apps/zuul/application.properties":
        mode => "0644",
        owner => 'java',
        group => 'java',
        source => 'puppet:///modules/spring_zuul/bootstrap.txt',
    }
remote_file { '/opt/apps/zuul/zuul.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://192.168.121.1/artifactory/zuul/zuul.jar',
  }
}
