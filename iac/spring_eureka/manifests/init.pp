# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include spring_eureka
class spring_eureka {
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/eureka':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
$content = "Versao 1.0"
  file { '/opt/apps/eureka/versao.txt':
    ensure  => file,
    content => $content,
    mode    => '0644',
    owner => 'java',
    group => 'java',
  }
file { "/etc/systemd/system/eureka.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/spring_eureka/eureka.service',
     }
package { 'maven':
        ensure => installed,
        name   => $maven,
    }

remote_file { '/opt/apps/eureka/eureka.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://192.168.121.1/artifactory/eureka/eureka.jar',
  }

}
