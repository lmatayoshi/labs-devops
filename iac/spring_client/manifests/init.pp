# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include spring_client
class spring_client {
exec { 'refresh_client':
  command => "/usr/bin/systemctl restart client",
  refreshonly => true,
}
exec { 'refresh_daemon_1':
  command => "/usr/bin/systemctl daemon-reload",
  refreshonly => true,
}
group { 'java':
       ensure => 'present',
       gid    => '501',
     }
user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }
file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { '/opt/apps/client':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/client.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/spring_client/client.service',
        notify => Exec['refresh_daemon_1'],
     }
package { 'maven':
        ensure => installed,
        name   => $maven,
    }
remote_file { "/opt/apps/client/application.properties":
        mode => "0644",
        owner => 'java',
        group => 'java',
        source => 'http://192.168.121.1/artifactory/client/application.properties',
        notify => Exec['refresh_client'],
    }
remote_file { '/opt/apps/client/client.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://192.168.121.1/artifactory/client/client.jar',
    notify => Exec['refresh_client'],
  }
}
