# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include spring_client_01
class spring_client_01 {
exec { 'refresh_client_01':
  command => "/usr/bin/systemctl restart client-01",
  refreshonly => true,
}
exec { 'refresh_daemon':
  command => "/usr/bin/systemctl daemon-reload",
  refreshonly => true,
}
file { '/opt/apps/client-01':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }
file { "/etc/systemd/system/client-01.service": 
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/spring_client_01/client-01.service',
        notify => Exec['refresh_daemon'],
     }
file { "/opt/apps/client-01/application.properties":
        mode => "0644",
        owner => 'java',
        group => 'java',
        source => 'puppet:///modules/spring_client_01/bootstrap.txt',
        notify => Exec['refresh_client_01'],
    }
remote_file { '/opt/apps/client-01/client-01.jar':
    ensure => latest,
    owner => 'java',
    group => 'java',
    source => 'http://192.168.121.1/artifactory/client-01/client-01.jar',
    notify => Exec['refresh_client_01'],
  }
}
