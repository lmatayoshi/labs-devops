#!/bin/bash
AKS_RESOURCE_GROUP=k8s-itau-group
AKS_CLUSTER_NAME=k8s-itau
ACR_RESOURCE_GROUP=k8s-itau-group
ACR_NAME=k8sacritau

# Get the id of the service principal configured for AKS
CLIENT_ID=$(az aks show --resource-group $AKS_RESOURCE_GROUP --name $AKS_CLUSTER_NAME --query "servicePrincipalProfile.clientId" --output tsv)

# Get the ACR registry resource id
ACR_ID=$(az acr show --name $ACR_NAME --resource-group $ACR_RESOURCE_GROUP --query "id" --output tsv)

# Create role assignment
az role assignment create --assignee $CLIENT_ID --role Reader --scope $ACR_ID

